/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#ifndef XAODTRACKING_TRACKBACKENDCONTAINER_H
#define XAODTRACKING_TRACKBACKENDCONTAINER_H

#include "xAODTracking/versions/TrackBackendContainer_v1.h"

namespace xAOD {
  typedef TrackBackendContainer_v1 TrackBackendContainer;
}

#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::TrackBackendContainer , 1077223759 , 1 )
#endif